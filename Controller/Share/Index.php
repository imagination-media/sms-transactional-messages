<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Controller\Share;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Session;
use ImaginationMedia\SmsNotifications\Model\Helper\GoogleShortener;
use ImaginationMedia\SmsNotifications\Model\Helper\Share as ShareHelper;
use ImaginationMedia\SmsNotifications\Model\Helper\Sms as SmsHelper;

class Index extends Action
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var GoogleShortener
     */
    protected $googleShortner;

    /**
     * @var ShareHelper
     */
    protected $shareHelper;

    /**
     * @var SmsHelper
     */
    protected $smsHelper;

    /**
     * Index constructor.
     * @param Context $context
     * @param Session $session
     * @param GoogleShortener $googleShortener
     * @param ShareHelper $shareHelper
     * @param SmsHelper $smsHelper
     */
    public function __construct(
        Context $context,
        Session $session,
        GoogleShortener $googleShortener,
        ShareHelper $shareHelper,
        SmsHelper $smsHelper
    ) {
        parent::__construct($context);
        $this->session = $session;
        $this->googleShortner = $googleShortener;
        $this->shareHelper = $shareHelper;
        $this->smsHelper = $smsHelper;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        if ($this->session->getQuoteId() &&
            $this->session->getQuote()->getItemsCount() > 0 &&
            $this->getRequest()->getParam("telephone")) {
            $phoneNumber = $this->getRequest()->getParam("telephone");
            $phoneNumber = $this->smsHelper->getPhoneNumber($phoneNumber);
            $url = $this->_url->getUrl(
                'smsnotifications/share/load',
                ['_secure' => true, 'id' => $this->session->getQuoteId()]
            );
            $messageContent = $this->shareHelper->getShareMessage($url);
            if ($this->googleShortner->isShortenerEnabled()) {
                $this->googleShortner->init();
                $messageContent = $this->googleShortner->prepareContent($messageContent);
            }
            if ($this->smsHelper->sendMessage($messageContent, $phoneNumber)) {
                $this->messageManager->addSuccessMessage(
                    __("SMS message successfully sent.")
                );
            } else {
                $this->messageManager->addErrorMessage(
                    __("Was not possible to send the SMS message.")
                );
            }
        } else {
            $this->messageManager->addErrorMessage(
                __("Was not possible to send the SMS message.")
            );
        }
        $this->_redirect('checkout/cart/index');
    }
}
