<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Controller\Share;

use ImaginationMedia\SmsNotifications\Model\Helper\Share;
use Magento\Catalog\Model\ProductRepository;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Quote\Model\Quote\Item;
use Magento\Quote\Model\QuoteRepository;

class Load extends Action
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var Share
     */
    protected $shareHelper;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * Load constructor.
     * @param Context $context
     * @param Session $session
     * @param QuoteRepository $quoteRepository
     * @param Share $shareHelper
     * @param Cart $cart
     * @param ProductRepository $productRepository
     */
    public function __construct(
        Context $context,
        Session $session,
        QuoteRepository $quoteRepository,
        Share $shareHelper,
        Cart $cart,
        ProductRepository $productRepository
    ) {
        parent::__construct($context);
        $this->session = $session;
        $this->quoteRepository = $quoteRepository;
        $this->shareHelper = $shareHelper;
        $this->cart = $cart;
        $this->productRepository = $productRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        if (array_key_exists('id', $data) && $data['id'] !== "") {
            if ($this->cart->getItemsCount() > 0) {
                foreach ($this->cart->getItems() as $item) {
                    $this->cart->removeItem($item->getId());
                }
            }
            try {
                $sharedQuote = $this->quoteRepository->get($data["id"]);
                /**
                 * Add same items to the current quote
                 * @var $item Item
                 */
                foreach ($sharedQuote->getAllItems() as $item) {
                    if ($item->getParentItemId() === null) {
                        $product = $this->productRepository->get($item->getSku());
                        $this->cart->addProduct($product, $item->getBuyRequest());
                    }
                }
                $this->cart->save();
                $this->messageManager->addSuccessMessage(
                    $this->shareHelper->getShareCartMessage()
                );
            } catch (\Exception $ex) {
                $this->messageManager->addErrorMessage(
                    __("Invalid cart, your cart link is expired.")
                );
            }
        } else {
            $this->messageManager->addErrorMessage(
                __("Invalid cart, your cart link is expired.")
            );
        }
        $this->_redirect('checkout/cart/index');
    }
}
