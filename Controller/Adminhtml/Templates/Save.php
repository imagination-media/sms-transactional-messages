<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Controller\Adminhtml\Templates;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use ImaginationMedia\SmsNotifications\Model\NotificationRepository;

class Save extends Action
{
    /**
     * @var NotificationRepository
     */
    private $repository;

    const ADMIN_RESOURCE = "ImaginationMedia_SmsNotifications::templates";

    /**
     * Save constructor.
     * @param Context $context
     * @param NotificationRepository $notificationRepository
     */
    public function __construct(Action\Context $context, NotificationRepository $notificationRepository)
    {
        parent::__construct($context);
        $this->repository = $notificationRepository;
    }

    /**
     * Save template action
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->validateRequest()) {
            if ($this->getRequest()->getParam("template_id") !== "") {
                $model = $this->repository->getNotificationById($this->getRequest()->getParam("template_id"));
            } else {
                $model = $this->repository->newModel();
                $model->setOrigTemplateCode($this->getRequest()->getParam("orig_template_code"));
            }
            $model->setContent($this->getRequest()->getParam("content"));
            if ($this->repository->save($model)) {
                $this->messageManager->addSuccessMessage(__('SMS template saved.'));
                return $resultRedirect->setPath(
                    '*/*/index'
                );
            } else {
                $this->messageManager->addErrorMessage(
                    __('Was not possible to save this sms template. Please try again.')
                );
                return $resultRedirect->setPath(
                    '*/*/edit',
                    ['template_id' => $model->getId(), '_current' => true]
                );
            }
        } else {
            $this->messageManager->addErrorMessage(
                __('Invalid request. Please check if you are trying to add more than one template to 
                same transactional message.')
            );
            return $resultRedirect->setPath(
                '*/*/index'
            );
        }
    }

    /**
     * Validate request
     * @return bool
     */
    private function validateRequest()
    {
        if ($this->getRequest()->getParam("content") !== "" &&
            $this->getRequest()->getParam("orig_template_code") !== "") {
            if ($this->getRequest()->getParam("template_id") !== "") {
                $template = $this->repository->getNotificationByTemplateCode(
                    $this->getRequest()->getParam("orig_template_code")
                );
                return ((int)$template->getTemplateId() === (int)$this->getRequest()->getParam("template_id"));
            } else {
                return (
                    $this->repository->hasTemplate(
                        "orig_template_code",
                        $this->getRequest()->getParam("orig_template_code")
                    )
                    === false);
            }
        }
    }
}
