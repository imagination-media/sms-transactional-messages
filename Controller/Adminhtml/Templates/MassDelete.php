<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Controller\Adminhtml\Templates;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use ImaginationMedia\SmsNotifications\Model\NotificationRepository;

class MassDelete extends Action
{
    /**
     * @var NotificationRepository
     */
    private $repository;

    const ADMIN_RESOURCE = "ImaginationMedia_SmsNotifications::templates";

    /**
     * Save constructor.
     * @param Context $context
     * @param NotificationRepository $notificationRepository
     */
    public function __construct(Action\Context $context, NotificationRepository $notificationRepository)
    {
        parent::__construct($context);
        $this->repository = $notificationRepository;
    }

    /**
     * Delete items
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($this->getRequest()->getParam("selected") && $this->getRequest()->getParam("selected") !== "") {
                //Delete the selected templates
                foreach ($this->getRequest()->getParam("selected") as $templateId) {
                    $this->repository->delete(
                        $this->repository->getNotificationById($templateId)
                    );
                }
            } elseif ($this->getRequest()->getParam("excluded") &&
                (bool)$this->getRequest()->getParam("selected") === false) {
                //Delete all templates
                foreach ($this->repository->getList() as $template) {
                    $template = $this->repository->getNotificationById($template->getTemplateId());
                    $this->repository->delete($template);
                }
            }
            $this->messageManager->addSuccessMessage(__('Templates deleted.'));
            return $resultRedirect->setPath(
                '*/*/index'
            );
        } catch (\Exception $ex) {
            $this->messageManager->addErrorMessage(
                __('Not possible to delete the templates. Error: '.$ex->getMessage())
            );
            return $resultRedirect->setPath(
                '*/*/index'
            );
        }
    }
}
