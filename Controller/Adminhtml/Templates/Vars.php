<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Controller\Adminhtml\Templates;

use Magento\Backend\App\Action;
use Magento\Email\Model\BackendTemplate;

class Vars extends Action
{

    const ADMIN_RESOURCE = "ImaginationMedia_SmsNotifications::templates";

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        if (key_exists("code", $params)) {
            $template = $this->initTemplate();
            $values = $template->getVariablesOptionArray(true);
            $values = $values['value'];
            //Prepare html
            $html = '<div class="template-attributes"><h2>'.__("Template variables").'</h2>';
            foreach ($values as $value) {
                $variable = $value['value'];
                /**
                 * @var $labelObject \Magento\Framework\Phrase
                 */
                $labelObject = $value['label'];
                $labelText = $labelObject->getArguments();
                $labelText = $labelText[0];
                $html.="<p>
                    <span><strong>$variable</strong></span>
                    <span>$labelText</span>
                </p>";
            }
            $html.='</div>';
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody(json_encode($html));
        }
    }

    /**
     * Load email template from request
     *
     * @return \Magento\Email\Model\BackendTemplate $model
     */
    protected function initTemplate()
    {
        $templateId = (string)$this->getRequest()->getParam('code');
        $template = $this->_objectManager->create(BackendTemplate::class);
        $template->setForcedArea($templateId);
        $template->loadDefault($templateId);
        $template->setData('orig_template_code', $templateId);
        $template->setData(
            'template_variables',
            json_encode($template->getVariablesOptionArray(true))
        );
        return $template;
    }
}
