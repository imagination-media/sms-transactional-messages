<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Plugin\Email\Model;

use ImaginationMedia\SmsNotifications\Model\Helper\Sms;
use ImaginationMedia\SmsNotifications\Model\NotificationRepository;
use Magento\Checkout\Model\Session;
use Magento\Customer\Model\AddressFactory;
use Magento\Customer\Model\CustomerFactory;
use Magento\Email\Model\BackendTemplateFactory;
use Magento\Email\Model\Template\Filter;
use Magento\Email\Model\Transport as CoreTransport;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\MailException;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Sales\Model\OrderFactory;
use Zend_Mail_Transport_Sendmail;

class Transport
{
    const XML_IS_ENABLED = "sms_notifications/settings/enabled";
    const XML_DISABLE_EMAILS = "sms_notifications/settings/disable_emails";
    const XML_PHONE_ATTRIBUTE = "sms_notifications/settings/phone_attribute";
    const XML_VALID_COUNTRIES = "sms_notifications/settings/valid_countries";

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var Zend_Mail_Transport_Sendmail
     */
    private $transport;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var NotificationRepository
     */
    private $notificationRepository;

    /**
     * @var QuoteFactory
     */
    private $quoteFactory;

    /**
     * @var Sms
     */
    private $sms;

    /**
     * @var BackendTemplateFactory
     */
    private $templateFactory;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var \Magento\Customer\Model\Address
     */
    private $customerAddress;

    /**
     * @var CustomerFactory
     */
    private $customerFactory;

    /**
     * @var AddressFactory
     */
    private $addressFactory;

    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * Transport constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Zend_Mail_Transport_Sendmail $transport
     * @param Session $session
     * @param NotificationRepository $notificationRepository
     * @param QuoteFactory $quoteFactory
     * @param Sms $sms
     * @param BackendTemplateFactory $backendTemplateFactory
     * @param Filter $filter
     * @param CustomerFactory $customerFactory
     * @param AddressFactory $addressFactory
     * @param OrderFactory $orderFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Zend_Mail_Transport_Sendmail $transport,
        Session $session,
        NotificationRepository $notificationRepository,
        QuoteFactory $quoteFactory,
        Sms $sms,
        BackendTemplateFactory $backendTemplateFactory,
        Filter $filter,
        CustomerFactory $customerFactory,
        AddressFactory $addressFactory,
        OrderFactory $orderFactory
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->transport = $transport;
        $this->session = $session;
        $this->notificationRepository = $notificationRepository;
        $this->quoteFactory = $quoteFactory;
        $this->sms = $sms;
        $this->templateFactory = $backendTemplateFactory;
        $this->filter = $filter;
        $this->customerFactory = $customerFactory;
        $this->addressFactory = $addressFactory;
        $this->orderFactory = $orderFactory;
    }

    /**
     * Send message on transactional messages
     * @param CoreTransport $subject
     * @param callable $proceed
     * @throws MailException
     */
    public function aroundSendMessage(CoreTransport $subject, callable $proceed)
    {
        $templateIdentifier = $subject->getMessage()->getTemplateIdentifier();
        if ($this->session->getQuoteId() !== null &&
            $this->quoteFactory->create()->getCollection()
                ->addFieldToFilter("entity_id", $this->session->getQuoteId())) {
                    $this->customerAddress = $this->quoteFactory->create()
                        ->load($this->session->getQuoteId())
                        ->getBillingAddress();
        } else {
            $this->getDefaultBillingAddress($subject->getMessage()->getRecipients()[0]);
        }

        if ($this->customerAddress !== null) {
            $country = $this->getCountry();
            $phoneNumber = $this->getPhoneNumber();
            $canSend = $this->canSendSMS($templateIdentifier, $country);
            $vars = $subject->getMessage()->getVars();
            $messageContent = $this->prepareContent($templateIdentifier, $vars);
            if ($canSend && $messageContent !== "" && ($phoneNumber !== null && $phoneNumber !== "")) {
                $canSend = $this->sms->sendMessage($messageContent, $phoneNumber);
            }
        } else {
            $canSend = false;
        }

        if (!$canSend || !$this->isEmailsDisabled()) {
            try {
                $isSetReturnPath = $this->scopeConfig->getValue(
                    CoreTransport::XML_PATH_SENDING_SET_RETURN_PATH,
                    ScopeInterface::SCOPE_STORE
                );
                $returnPathValue = $this->scopeConfig->getValue(
                    CoreTransport::XML_PATH_SENDING_RETURN_PATH_EMAIL,
                    ScopeInterface::SCOPE_STORE
                );

                if ($isSetReturnPath == '1') {
                    $subject->getMessage()->setReturnPath($subject->getMessage()->getFrom());
                } elseif ($isSetReturnPath == '2' && $returnPathValue !== null) {
                    $subject->getMessage()->setReturnPath($returnPathValue);
                }
                $this->transport->send($subject->getMessage());
            } catch (\Exception $e) {
                throw new MailException(__($e->getMessage()), $e);
            }
        }
    }

    /**
     * Check if transactional sms messages are enabled
     * @return boolean
     */
    private function isEnabled()
    {
        return $this->scopeConfig->getValue(self::XML_IS_ENABLED, ScopeInterface::SCOPE_STORE);
    }

    /**
     * Check if transactional emails will be disabled
     * @return boolean
     */
    private function isEmailsDisabled()
    {
        return $this->scopeConfig->getValue(self::XML_DISABLE_EMAILS, ScopeInterface::SCOPE_STORE);
    }

    /**
     * Get the attribute used as mobile phone
     * @return int
     */
    private function getPhoneAttribute()
    {
        return $this->scopeConfig->getValue(self::XML_PHONE_ATTRIBUTE, ScopeInterface::SCOPE_STORE);
    }

    /**
     * Return all valid countries
     * @return string
     */
    private function getValidCountries()
    {
        return $this->scopeConfig->getValue(self::XML_VALID_COUNTRIES, ScopeInterface::SCOPE_STORE);
    }

    /**
     * Check if is allowed to send the SMS message
     * @param $templateIdentifier
     * @param $country
     * @return bool
     */
    private function canSendSMS($templateIdentifier, $country)
    {
        $enabled = $this->isEnabled();
        $phoneAttribute = $this->getPhoneAttribute();
        $validCountries = explode(',', $this->getValidCountries());
        $hasTemplate = $this->notificationRepository->hasTemplate("orig_template_code", $templateIdentifier);
        $canSend = true;
        if (!$enabled) {
            $canSend = false;
        }
        if ($phoneAttribute === "") {
            $canSend = false;
        }
        if (!in_array($country, $validCountries)) {
            $canSend = false;
        }
        if (!$hasTemplate) {
            $canSend = false;
        }
        if ($this->customerAddress === null) {
            $canSend = false;
        }
        return $canSend;
    }

    /**
     * Return sms text content
     * @param string $templateCode
     * @return string
     */
    private function prepareContent($templateCode, $vars)
    {
        try {
            /**
             * @var $template \Magento\Email\Model\Template
             */
            $template = $this->templateFactory->create();
            $template->setForcedArea($templateCode);
            $template->loadDefault($templateCode);
            $template->setVars($vars);
            $template->processTemplate();
            $smsTemplate = $this->notificationRepository->getNotificationByTemplateCode($templateCode);
            $content = $template->getTemplateFilter()->filter($smsTemplate->getContent());
            //Use Google Shortener URL (if enabled)
            $content = $this->sms->prepareContent($content);
            return $content;
        } catch (\Exception $ex) {
            return "";
        }
    }

    /**
     * Load default billing address
     * @param string $email
     * @return null
     */
    private function getDefaultBillingAddress($email)
    {
        if ($this->customerFactory->create()
                ->getCollection()
                ->addAttributeToFilter("email", $email)
                ->count() > 0) {
            $customer = $this->customerFactory->create()
                ->getCollection()
                ->addAttributeToFilter("email", $email)
                ->getFirstItem();
            try {
                $this->customerAddress = $this->addressFactory->create()->load($customer->getDefaultBilling());
            } catch (\Exception $ex) {
                return null;
            }
        } else {
            if ($this->orderFactory->create()
                     ->getCollection()
                     ->addFieldToFilter("customer_email", $email)
                     ->count() > 0) {
                        /**
                         * @var $lastOrder \Magento\Sales\Model\Order
                         */
                        $lastOrder = $this->orderFactory->create()
                            ->getCollection()
                            ->addFieldToFilter("customer_email", $email)
                            ->getLastItem();
                        $this->customerAddress = $lastOrder->getBillingAddress();
            } else {
                return null;
            }
        }
    }

    /**
     * Return address country id
     * @return string
     */
    private function getCountry()
    {
        return $this->customerAddress->getCountryId();
    }

    /**
     * Return the customer phone number used to send the sms
     * @return string
     */
    private function getPhoneNumber()
    {
        $phoneAttribute = $this->getPhoneAttribute();
        $countryId = $this->getCountry();
        $ddi = $this->sms->getDDINumber($countryId);
        $phone = $this->customerAddress->getData($phoneAttribute);
        $phoneNumber = $ddi . $phone;
        return $phoneNumber;
    }
}
