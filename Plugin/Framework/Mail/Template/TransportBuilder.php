<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Plugin\Framework\Mail\Template;

use Magento\Framework\Mail\Template\TransportBuilder as CoreBuilder;

class TransportBuilder extends CoreBuilder
{
    /**
     * Add template identifier in the transport object
     * @return \Magento\Framework\Mail\TransportInterface
     */
    public function getTransport()
    {
        $templateIdentifier = $this->templateIdentifier;
        $this->prepareMessage();
        $this->message->setTemplateIdentifier($templateIdentifier);
        $this->message->setVars($this->templateVars);
        $mailTransport = $this->mailTransportFactory->create(
            [
                'message' => $this->message
            ]
        );
        $this->reset();
        return $mailTransport;
    }
}
