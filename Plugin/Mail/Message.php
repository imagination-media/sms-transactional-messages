<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Plugin\Mail;

use Magento\Framework\Mail\Message as CoreMessage;

class Message extends CoreMessage
{
    /**
     * @var string
     */
    protected $templateIdentifier;

    /**
     * @var array
     */
    protected $vars;

    /**
     * Return template identifier
     * @return string
     */
    public function getTemplateIdentifier()
    {
        return $this->templateIdentifier;
    }

    /**
     * Set a new template identifier
     * @param $templateIdentifier
     */
    public function setTemplateIdentifier($templateIdentifier)
    {
        $this->templateIdentifier = $templateIdentifier;
    }

    /**
     * Set template vars
     * @param array $vars
     */
    public function setVars($vars)
    {
        $this->vars = $vars;
    }

    /**
     * Return template vars
     * @return array
     */
    public function getVars()
    {
        return $this->vars;
    }
}
