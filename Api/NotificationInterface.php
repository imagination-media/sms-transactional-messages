<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Api;

use ImaginationMedia\SmsNotifications\Model\Notification;
use ImaginationMedia\SmsNotifications\Model\ResourceModel\Notification\Collection;

interface NotificationInterface
{
    /**
     * Load notification model by template_id
     * @param int|string $id
     * @return Notification
     */
    public function getNotificationById($id);

    /**
     * Load notification model by template code
     * @param string $templateCode
     * @return Notification
     */
    public function getNotificationByTemplateCode($templateCode);

    /**
     * Return notification content loading by field and value
     * @param string $field
     * @param string $value
     * @return string
     */
    public function getNotificationContent($field, $value);

    /**
     * Check if there is available template by field
     * @param string $field
     * @param string|int $value
     * @return boolean
     */
    public function hasTemplate($field, $value);

    /**
     * Return a collection of items
     * @return Collection
     */
    public function getList();

    /**
     * Return a new notification object
     * @return Notification
     */
    public function newModel();

    /**
     * Save notification model
     * @param Notification $model
     * @return bool
     */
    public function save($model);

    /**
     * Delete notification model
     * @param Notification $model
     * @return bool
     */
    public function delete($model);
}
