<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Block\Adminhtml\Templates\Edit;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use ImaginationMedia\SmsNotifications\Model\NotificationFactory;

class Save implements ButtonProviderInterface
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var NotificationFactory
     */
    protected $notification;

    /**
     * SaveButton constructor.
     * @param Context $context
     * @param NotificationFactory $notificationFactory
     */
    public function __construct(
        Context $context,
        NotificationFactory $notificationFactory
    ) {
        $this->context = $context;
        $this->notification = $notificationFactory;
    }

    /**
     * @return null
     */
    public function getPageId()
    {
        try {
            return $this->notification->load(
                $this->context->getRequest()->getParam('template_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * @param string $route
     * @param array $params
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }

    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Save'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];
    }
}
