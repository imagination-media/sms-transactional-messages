<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Block;

use Magento\Framework\View\Element\Template;
use ImaginationMedia\SmsNotifications\Model\Helper\Share;

class ShareCart extends Template
{
    /**
     * @var Share
     */
    protected $share;

    /**
     * ShareCart constructor.
     * @param Template\Context $context
     * @param Share $share
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Share $share,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->share = $share;
    }

    /**
     * Check if share cart is enabled
     * @return bool
     */
    public function isEnabled()
    {
        return $this->share->isShareEnabled();
    }

    /**
     * Return the share cart controller url
     * @return string
     */
    public function getFormUrl()
    {
        return $this->getUrl('smsnotifications/share/index', ['_secure' => true]);
    }
}
