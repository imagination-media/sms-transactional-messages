/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

define([
    'uiComponent',
    'jquery',
    'ko',
    'underscore',
    'mageUtils',
    'Magento_Ui/js/lib/collapsible',
    'Magento_Ui/js/modal/alert',
    'mage/translate'
], function (Component, $, ko, _, utils, Collapsible, alert) {
    'use strict';
    /**
     * Initialize component
     */

    var formUrl = undefined;

    return Collapsible.extend({
        defaults: {
            template: 'ImaginationMedia_SmsNotifications/share'
        },

        initialize: function () {
            this._super();
            formUrl = this.formUrl;
        },
        /**
         * Clear telephone field
         * @param number
         */
        resetSms: function (number) {
            $('.telephone-number').val('');
        },

        /**
         * Show the popup
         */
        openShareCart: function () {
            $('.share-cart-container').addClass("active");
        },

        /**
         * Add the number to the telephone field
         * @param number
         */
        selectNumber: function (number) {
            if ($('.telephone-number').val() !== "1234569780") {
                var currentNumber = $('.telephone-number').val();
                currentNumber = currentNumber + '' + number;
                $('.telephone-number').val(currentNumber);
                $('.telephone-number').attr("value", currentNumber);
            } else {
                $('.telephone-number').val(number);
                $('.telephone-number').attr("value", number);
            }
        },

        /**
         * Redirect to the send sms controller
         */
        sendSms: function () {
            var telephone = $('.telephone-number').val();
            if (telephone !== "") {
                var formUrl = this.getFormUrl();
                $('.telephone-number').removeAttr("disabled");
                $('.share-cart-form').attr("action", formUrl);
                $('.share-cart-form').submit();
            } else {
                alert({
                    title: $.mage.__('Invalid Phone'),
                    content: '<p>' + $.mage.__("Please type a valid phone.") + '</p>',
                    actions: {
                        always: function () {
                        }
                    }
                });
            }
        },

        /**
         * Return share cart form url
         * @returns string
         */
        getFormUrl: function () {
            return this.formUrl;
        },

        /**
         * Close PopUp
         */
        closePopUp: function () {
            $('.share-cart-container').removeClass("active");
        }
    });
});