<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Model\ResourceModel\Notification;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use ImaginationMedia\SmsNotifications\Model\NotificationFactory;

class DataProvider extends AbstractDataProvider
{
    protected $collection;
    protected $dataPersist;
    protected $loadedData;

    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param NotificationFactory $notification
     * @param DataPersistorInterface $dataPersist
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        NotificationFactory $notification,
        DataPersistorInterface $dataPersist,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $notification->create()->getCollection();
        $this->dataPersist = $dataPersist;
        $this->meta = $this->prepareMeta($this->meta);
    }

    /**
     * @param array $meta
     * @return array
     */
    public function prepareMeta(array $meta)
    {
        return $meta;
    }

    /**
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $notification) {
            $this->loadedData[$notification->getTemplateId()] = $notification->getData();
        }

        $data = $this->dataPersist->get('smsnotifications_templates');
        if (!empty($data)) {
            $notification = $this->collection->getNewEmptyItem();
            $notification->setData($data);
            $this->loadedData[$notification->getTemplateId()] = $notification->getData();
            $this->dataPersist->clear('smsnotifications_templates');
        }
        return $this->loadedData;
    }
}
