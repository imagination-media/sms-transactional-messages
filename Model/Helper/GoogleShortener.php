<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Model\Helper;

use Magento\Framework\App\AreaList;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Store\Model\ScopeInterface;

class GoogleShortener extends AbstractModel
{
    const XML_GOOGLE_ENABLED = "sms_notifications/google_shortener/enable";
    const XML_GOOGLE_API_KEY = "sms_notifications/google_shortener/api_key";

    /**
     * @var string
     */
    protected $apiUrl;

    /**
     * @var EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var AreaList
     */
    protected $areaList;

    /**
     * GoogleShortener constructor.
     * @param Context $context
     * @param Registry $registry
     * @param EncryptorInterface $encryptor
     * @param ScopeConfigInterface $scopeConfig
     * @param AreaList $areaList
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        EncryptorInterface $encryptor,
        ScopeConfigInterface $scopeConfig,
        AreaList $areaList,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->encryptor = $encryptor;
        $this->scopeConfig = $scopeConfig;
        $this->areaList = $areaList;
    }

    /**
     * Init Google Shortener helper
     * @param string $apiURL
     */
    public function init($apiURL = 'https://www.googleapis.com/urlshortener/v1/url')
    {
        $this->apiUrl = $apiURL . '?key=' . $this->getShortnerApi();
    }

    /**
     * Get Google Shortener Api Key
     * @return string
     */
    public function getShortnerApi()
    {
        $value = $this->scopeConfig->getValue(self::XML_GOOGLE_API_KEY, ScopeInterface::SCOPE_STORE);
        return $this->encryptor->decrypt($value);
    }

    /**
     * Check if Google shortener is enabled
     * @return bool
     */
    public function isShortenerEnabled()
    {
        return $this->scopeConfig->getValue(self::XML_GOOGLE_ENABLED, ScopeInterface::SCOPE_STORE);
    }

    /**
     * Shorten an url
     * @param string $url
     * @return bool
     */
    public function shorten($url)
    {
        $response = $this->send($url);
        return isset($response['id']) ? $response['id'] : false;
    }

    /**
     * Expand an URL
     * @param string $url
     * @return bool
     */
    public function expand($url)
    {
        $response = $this->send($url, false);
        return isset($response['longUrl']) ? $response['longUrl'] : false;
    }

    /**
     * @param string $url
     * @param bool $shorten
     * @return mixed
     */
    public function send($url, $shorten = true)
    {
        $ch = curl_init();
        if ($shorten) {
            curl_setopt($ch, CURLOPT_URL, $this->apiUrl);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array("longUrl" => $url)));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        } else {
            curl_setopt($ch, CURLOPT_URL, $this->apiUrl . '&shortUrl=' . $url);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, true);
    }

    /**
     * Return the prepared content with short urls
     * @param $content
     * @return string
     */
    public function prepareContent($content)
    {
        $match = array();
        $content = str_replace($this->areaList->getFrontName('adminhtml') . "/", "", $content);
        preg_match_all('#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', $content, $match);
        $match = $this->fixUrls($match);
        if (count($match) === 0) {
            return $content;
        } else {
            $newUrls = array();
            foreach ($match as $url) {
                $shortenUrl = $this->shorten($url);
                if ($shortenUrl !== null && $shortenUrl !== "") {
                    $newUrls[] = array(
                        'url' => $url,
                        'shorten' => $shortenUrl
                    );
                }
            }
            foreach ($newUrls as $urls) {
                $content = str_replace($urls['url'], $urls['shorten'], $content);
            }
            return $content;
        }
    }

    /**
     * Return valid urls
     * @param array $urls
     * @return array
     */
    private function fixUrls($urls)
    {
        $finalUrls = array();
        foreach ($urls as $url) {
            if (is_array($url)) {
                foreach ($url as $subUrl) {
                    if (filter_var($subUrl, FILTER_VALIDATE_URL)) {
                        $finalUrls[] = $subUrl;
                    }
                }
            } else {
                if (filter_var($url, FILTER_VALIDATE_URL)) {
                    $finalUrls[] = $url;
                }
            }
        }
        return $finalUrls;
    }
}
