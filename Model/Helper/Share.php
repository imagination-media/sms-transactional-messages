<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Model\Helper;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Store\Model\ScopeInterface;

class Share extends AbstractModel
{
    const XML_SHARE_ENABLED = "sms_notifications/share_cart/enable";
    const XML_SHARE_MESSAGE = "sms_notifications/share_cart/message";
    const XML_SHARE_CART_MESSAGE = "sms_notifications/share_cart/cart_message";
    const SHARE_URL_ALIAS = "[final_url]";

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Share constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ScopeConfigInterface $scopeConfig
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $scopeConfig,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Check if share cart is enabled
     * @return bool
     */
    public function isShareEnabled()
    {
        return $this->scopeConfig->getValue(self::XML_SHARE_ENABLED, ScopeInterface::SCOPE_STORE);
    }

    /**
     * Get share message used on share sms
     * @param $url
     * @return string
     */
    public function getShareMessage($url)
    {
        $message = $this->scopeConfig->getValue(self::XML_SHARE_MESSAGE, ScopeInterface::SCOPE_STORE);
        $message = str_replace(self::SHARE_URL_ALIAS, $url, $message);
        return $message;
    }

    /**
     * Return the message that will be used on Magento cart after the redirect
     * @return string
     */
    public function getShareCartMessage()
    {
        return $this->scopeConfig->getValue(self::XML_SHARE_CART_MESSAGE, ScopeInterface::SCOPE_STORE);
    }
}
