<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Model\Config\Source\Customer\Address;

use Magento\Framework\Option\ArrayInterface;
use Magento\Customer\Model\Address;

class Attributes implements ArrayInterface
{
    /**
     * @var Address
     */
    protected $address;

    /**
     * Attributes constructor.
     * @param Address $address
     */
    public function __construct(Address $address)
    {
        $this->address = $address;
    }

    /**
     * Return available address attributes
     * @return array
     */
    public function toOptionArray()
    {
        $addressAttributes = array();
        foreach ($this->address->getAttributes() as $attribute) {
            $addressAttributes[] = array(
                'value' => $attribute->getAttributeCode(),
                'label' => $attribute->getDefaultFrontendLabel()
            );
        }
        return $addressAttributes;
    }
}
