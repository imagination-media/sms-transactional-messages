<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Email\Model\Template\Config as EmailConfig;

class Templates implements ArrayInterface
{
    /**
     * @var EmailConfig
     */
    private $emailConfig;

    /**
     * Templates constructor.
     * @param EmailConfig $emailConfig
     */
    public function __construct(EmailConfig $emailConfig)
    {
        $this->emailConfig = $emailConfig;
    }

    /**
     * Return available templates
     * @return array
     */
    public function toOptionArray()
    {
        return $this->emailConfig->getAvailableTemplates();
    }
}
