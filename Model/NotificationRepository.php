<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Model;

use ImaginationMedia\SmsNotifications\Api\NotificationInterface;
use ImaginationMedia\SmsNotifications\Model\Notification;
use ImaginationMedia\SmsNotifications\Model\ResourceModel\Notification\Collection;
use ImaginationMedia\SmsNotifications\Model\ResourceModel\Notification as ResourceNotification;
use ImaginationMedia\SmsNotifications\Model\NotificationFactory;

class NotificationRepository implements NotificationInterface
{
    /**
     * @var NotificationFactory
     */
    protected $notificationFactory;

    /**
     * @var ResourceNotification
     */
    private $resourceNotification;

    public function __construct(NotificationFactory $notificationFactory, ResourceNotification $resourceNotification)
    {
        $this->notificationFactory = $notificationFactory;
        $this->resourceNotification = $resourceNotification;
    }

    /**
     * Load notification model by template_id
     * @param int|string $id
     * @return Notification
     */
    public function getNotificationById($id)
    {
        if ($this->notificationFactory->create()
                ->getCollection()
                ->addFieldToFilter("template_id", $id)
                ->count() > 0) {
                    return $this->notificationFactory->create()
                        ->load($id);
        }
    }

    /**
     * Load notification model by template code
     * @param string $templateCode
     * @return Notification
     */
    public function getNotificationByTemplateCode($templateCode)
    {
        return $this->notificationFactory->create()
            ->getCollection()
            ->addFieldToFilter("orig_template_code", $templateCode)
            ->getFirstItem();
    }

    /**
     * Return notification content loading by field and value
     * @param string $field
     * @param string $value
     * @return string
     */
    public function getNotificationContent($field, $value)
    {
        if ($this->notificationFactory->create()
                ->getCollection()
                ->addFieldToFilter($field, $value)
                ->count() > 0) {
            return $this->notificationFactory->create()
                ->getCollection()
                ->addFieldToFilter($field, $value)
                ->getFirsItem()
                ->getContent();
        }
    }

    /**
     * Check if there is available template by field
     * @param string $field
     * @param string|int $value
     * @return boolean
     */
    public function hasTemplate($field, $value)
    {
        return ($this->notificationFactory->create()
                ->getCollection()
                ->addFieldToFilter($field, $value)
                ->count() > 0);
    }

    /**
     * Return a collection of items
     * @return Collection
     */
    public function getList()
    {
        return $this->notificationFactory->create()
            ->getCollection();
    }

    /**
     * Return a new notification object
     * @return Notification
     */
    public function newModel()
    {
        return $this->notificationFactory->create();
    }

    /**
     * Save notification model
     * @param Notification $model
     * @return bool
     */
    public function save($model)
    {
        try {
            $this->resourceNotification->save($model);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * Delete notification model
     * @param Notification $model
     * @return bool
     */
    public function delete($model)
    {
        try {
            $this->resourceNotification->delete($model);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }
}
