<?php

/**
 * Sms Notification
 *
 * Notify customers using sms messages instead of emails
 *
 * @package ImaginationMedia\SmsNotifications
 * @author Igor Ludgero Miura <igor@imaginationmedia.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://www.imaginationmedia.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\SmsNotifications\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use ImaginationMedia\SmsNotifications\Model\ResourceModel\Notification as ResourceNotification;

class Notification extends AbstractModel
{
    /**
     * Cotacoes constructor.
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $registry, null, null, $data);
    }

    /**
     * _Construct method
     */
    protected function _construct()
    {
        $this->_init(ResourceNotification::class);
    }
}
